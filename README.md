# Telephony Service

## Overview

Telephony service allows respective clients access to the Handsfree Profile via the oFono package

## Verbs

| Name                | Description                                     | JSON Response                               |
|---------------------|-------------------------------------------------|---------------------------------------------|
| subscribe           | subscribe to telephony events                   | *Request:* {"value": "callStateChanged"}    |
| unsubscribe         | unsubscribe to telephony events                 | *Request:* {"value": "callStateChanged"}    |
| dial                | dial respective number                          | *Request:* {"value": "15035551212"}         |
| last_dial           | dial last dialed number                         |                                             |
| hangup_all          | hang up all active calls                        |                                             |
| send_tones          | send tone through the active call               | *Request:* {"value": "1"}                   |
| hangup              | hangup an active call or reject incoming call. this verb does not hang up all calls if exist more than one active call   | *Request:* {"id": "voicecall01"}                   |
| answer              | answer incoming call                            |                                             |
| hold_and_answer     | Puts the current call on hold and answers the currently waiting call |                        |
| release_and_answer  | Releases currently active call and activates any currently held calls |                       |
| hangup_multiparty   | Hangs up the multi-party call                   |                                             |
| create_multiparty   | Joins active and held calls together into a multi-party call |                                |
| swap_calls	      | Swaps Active and Held calls                     |                                             |
| get_calls  	      | get active call properties                      | *Response:* {"active calls": [ {"Object Path": "/hfp/org/bluez/hci0/dev_xx_xx_xx_xx_xx_xx/voicecall01", "State": "dialing", "LineIdentification": "123456789", "Name": "", "Multiparty": false, "RemoteHeld": false, "RemoteMultiparty": false, "Emergency": false } ] }                                            |
| get_battery_level   | getting battery level of connected phone device |                                             |
| get_network_registration   | getting network registration of connected phone device | *Response:* {"Status": "registered","Mode": "auto-only","Name": "Irancell","Strength": 20} |

## Events

| Name                | Description                             | JSON Event Data                             |
|---------------------|-----------------------------------------|---------------------------------------------|
| callStateChanged    | Call status change event                |   see callStateChanged event                |
| dialingCall         | Outgoing call events                    | {"colp": "3305551212"}                      |
| incomingCall        | Incoming call events                    | {"clip": "3305551212"}                      |
| waitingCall         | waiting call events                     | {"colp": "3305551212"}                      |
| terminatedCall      | Terminated call event                   | *empty JSON response*                       |
| online              | Connected status of Handsfree Profile   | {"connected": true}                         |
| battery             | Connected status of Handsfree Profile   | {"battery level": 2}                        |
| signal              | Connected status of Handsfree Profile   | {"signal level": 20}                        |

### callStateChanged Event

This event has one field called *parameter* with one of the following states (see **doc/voicecall-api.txt** in oFono for further reference):

| Name           | Description                       |
|----------------|-----------------------------------|
| active         | Call is active                    |
| held           | Call is on hold                   |
| dialing        | Call is being dialed              |
| alerting       | Remote party is being alerted     |
| incoming       | Incoming call in progress         |
| waiting        | Call is waiting                   |
| disconnected   | Call is disconnected              |

