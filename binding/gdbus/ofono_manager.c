/*
 * Copyright (C) 2017-2018 Konsulko Group
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE

#include <string.h>

#include "ofono_manager.h"
#include "ofono_manager_interface.h"

struct ofono_manager_modem
{
	const gchar *address;
	const gchar *path;
	const gchar *name;
	const gchar *type;
	gboolean powered;
	gboolean online;
	gboolean valid;
};

static OrgOfonoManager *manager;
static struct ofono_manager_modem default_modem;

void ofono_manager_invalidate_default_modem()
{
	default_modem.valid = FALSE;
}

int ofono_manager_set_default_modem(const char *address)
{
	GVariant *out_arg = NULL, *next, *value;
	GError *error = NULL;
	gchar *path, *key;
	const gchar *name = NULL, *type = NULL, *serial = NULL;
	gboolean powered = FALSE, online = FALSE;
	GVariantIter *iter, *iter2 = NULL;

	/* Fetch all visible modems */
	org_ofono_manager_call_get_modems_sync(manager, &out_arg, NULL, &error);
	if (error != NULL)
		return -1;

	g_variant_get(out_arg, "a(oa{sv})", &iter);
	/* Iterate over each modem */
	while ((next = g_variant_iter_next_value(iter))) {
		g_variant_get(next, "(oa{sv})", &path, &iter2);
		while (g_variant_iter_loop(iter2, "{sv}", &key, &value)) {
			if (!strcmp(key, "Name"))
				name = g_variant_get_string(value, NULL);
			else if (!strcmp(key, "Online"))
				online = g_variant_get_boolean(value);
			else if (!strcmp(key, "Powered"))
				powered = g_variant_get_boolean(value);
			else if (!strcmp(key, "Serial"))
				serial = g_variant_get_string(value, NULL);
			else if (!strcmp(key, "Type"))
				type = g_variant_get_string(value, NULL);
		}

		/* If address is set then searching for hfp modem */
		if (address && g_strcmp0(type, "hfp"))
			continue;

		/* If address is NULL then use the first modem as default,
		 * and if not then continue if doesn't match address.
		 */
		if (address && g_strcmp0(address, serial))
			continue;

		/* If powered, and online then set as default */
		if (powered && online) {
			default_modem.address = serial;
			default_modem.path = path;
			default_modem.name = name;
			default_modem.type = type;
			default_modem.powered = powered;
			default_modem.online = online;
			default_modem.valid = TRUE;
			AFB_NOTICE("New modem: %s (%s)", name, serial);
			break;
		}
	}

	return 0;
}

int ofono_manager_init()
{
	int ret = 0;

	if (manager) {
		AFB_ERROR("Ofono Manager already initialized\n");
		return -1;
	}

	manager = org_ofono_manager_proxy_new_for_bus_sync(
		G_BUS_TYPE_SYSTEM, G_DBUS_PROXY_FLAGS_NONE,
		"org.ofono", "/", NULL, NULL);

	if (!manager) {
		AFB_ERROR("Ofono Manager not initialized\n");
		return -1;
	}


	return ret;
}

const gchar *ofono_manager_get_default_modem_address(void)
{
	if (!manager) {
		AFB_ERROR("Ofono Manager not initialized\n");
	}

	return default_modem.address;
}

const gchar *ofono_manager_get_default_modem_path(void)
{
	if (!manager) {
		AFB_ERROR("Ofono Manager not initialized\n");
	}

	return default_modem.path;
}

const gchar *ofono_manager_get_default_modem_name(void)
{
	if (!manager) {
		AFB_ERROR("Ofono Manager not initialized\n");
	}

	return default_modem.name;
}

const gchar *ofono_manager_get_default_modem_type(void)
{
	if (!manager) {
		AFB_ERROR("Ofono Manager not initialized\n");
	}

	return default_modem.type;
}

gboolean ofono_manager_get_default_modem_powered(void)
{
	if (!manager) {
		AFB_ERROR("Ofono Manager not initialized\n");
	}

	return default_modem.powered;
}

gboolean ofono_manager_get_default_modem_online(void)
{
	if (!manager) {
		AFB_ERROR("Ofono Manager not initialized\n");
	}

	return default_modem.online;
}

gboolean ofono_manager_get_default_modem_valid(void)
{
	if (!manager) {
		AFB_ERROR("Ofono Manager not initialized\n");
	}

	return default_modem.valid;
}

static json_object *gvariant_to_json(GVariant *value)
{
	const gchar *type = g_variant_get_type_string(value);
	json_object *res = NULL;
	json_object *obj = NULL;
	switch (*type) {
	case 'o':
	case 's':
		res = json_object_new_string(g_variant_get_string(value, NULL));
		break;
	case 'b':
		res = json_object_new_int(g_variant_get_boolean(value));
		break;
	case 'u':
		res = json_object_new_int64(g_variant_get_uint32(value));
		break;
	case 'y':
		res = json_object_new_int((int)g_variant_get_byte(value));
		break;
	case 'a':
		if(g_strcmp0(type, "as"))
			break;
		const gchar *arr_str;
		GVariantIter i;
		obj = json_object_new_array();
		g_variant_iter_init(&i, value);
		while (g_variant_iter_next(&i, "s", &arr_str))
			json_object_array_add(obj, json_object_new_string(arr_str));
		res = obj;
	default:
		break;
	}
	return res;
}

json_object * ofono_get_properties(GDBusConnection *conn, const char *path, const char *interface, GError **error)
{
	GVariant *reply = NULL;
	GVariantIter *array;
	GVariant *var = NULL;
	const gchar *key = NULL;
	json_object *jprop = NULL;

	reply = g_dbus_connection_call_sync(conn,
										OFONO_SERVICE, path, interface, "GetProperties",
										NULL, NULL, G_DBUS_CALL_FLAGS_NONE,
										(120 * 1000), NULL, error);
	if (!reply)
		return NULL;

	g_variant_get(reply, "(a{sv})", &array);
	jprop = json_object_new_object();
	while (g_variant_iter_loop(array, "{sv}", &key, &var)) {
		json_object_object_add(jprop,key,gvariant_to_json(var));
	}
	g_variant_iter_free(array);
	g_variant_unref(reply);
	return jprop;
}

json_object *ofono_get_property(const char *path, const char *interface, const char *name, GError **error)
{
	GDBusConnection *conn = g_bus_get_sync(G_BUS_TYPE_SYSTEM, NULL, error);
	json_object *jprop;
	json_object *jret = json_object_new_object();
	jprop = ofono_get_properties(conn,path,interface,error);
	json_object_object_foreach(jprop, key, jval) {
		if (name) {
			if (!g_strcmp0(key, name)) {
				return jval;
			}
		}
		json_object_object_add(jret,key,jval);
	}
	return jret;
}